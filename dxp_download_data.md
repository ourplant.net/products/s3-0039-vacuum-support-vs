Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/01_operating_manual/S3-0039_B_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/01_operating_manual/S3-0039_B_Operating%20Manual.pdf)                      |
| assembly drawing         | [de](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/02_assembly_drawing/S3-0039-A-ZNB_VS.pdf)                 |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/03_circuit_diagram/30-0195-EPLAN-A.pdf)                  |
| maintenance instructions |[de](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/04_maintenance_instructions/S3-0039_A_Wartungsanweisungen.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/04_maintenance_instructions/S3-0039_A_Maintenance%20instructions.pdf)                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/05_spare_parts/S3-0039_B1_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0039-vacuum-support-vs/-/raw/main/05_spare_parts/S3-0039_B1_EVL_engl.pdf)                  |

